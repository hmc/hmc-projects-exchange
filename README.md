# HMC projects exchange marketplace

### What is this?
This is an exchange place for ideas and raw proposals, to prepare for the 
[currently ongoing project call](https://helmholtz-metadaten.de/index.php/1st-hmc-project-call-published/) of the [Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/). 

Deadline for project proposals is **Oct 14, 2020**.

### How to do?
Check and formulate ideas [**in the issue tracker**](https://gitlab.hzdr.de/hmc/hmc-projects-exchange/-/issues).

You can read all issues without login.

If you want to add/change anything, you need to sign in (Helmholtz AAI). 
This should work for Helmholtz and most scientific institutions,  as well as ORCID and social media accounts.

Labels can be applied and arbitrarily combined:
* 6 Helmholtz Research areas for which you think your idea may be of interest
* "Offer" - if you have a processing methodology or pipeline you would like to adapt to new research areas
* "Demand" - if you have a specific imaging related problem in you research area that you would like to solve with new technologies

You can set due dates and, of course, add formatting, external links and files.

# About
This service is made available by the [Helmholtz-Zentrum Dresden-Rossendorf (HZDR)](https://www.hzdr.de/ ) as a pilot project with the [HIFIS platform](https://www.hifis.net/).
